$(document).ready(function() {

  $('.home-about__reviews-cards').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
  });

  $('.good__for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.good__nav'
  });

  $('.good__nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.good__for',
    dots: true,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
  });

});